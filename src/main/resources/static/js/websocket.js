

socket.onopen = function() {
	console.log("Connected!");
};

socket.onerror = function(errorEvent) {
	console.log("Connection closed!");
};


socket.onclose = function(closeEvent) {
	console.log("Connection closed!");
};


socket.onmessage = function(messageEvent) {
	if (messageEvent.data.startsWith("<p class=")) { // it should be log-data

		if (contentNew) {
			clusterize.update(messageEvent.data.split("SEPARATOR"));
		}
		else {
			clusterize.append(messageEvent.data.split("SEPARATOR"));
		}

		contentNew = false;
		return;
	}

	// id should be json-data
	var jsondata = JSON.parse(messageEvent.data);
	if (jsondata.action != null && jsondata.data != null) {
		if (jsondata.action === "clearall") {
			document.getElementById('content').innerHTML = "";
		}
		if (jsondata.action === "notify") {
			if (window.Notification && Notification.permission !== "denied") {
				Notification.requestPermission(function(status) {
					var notification = new Notification('LogViewer: ', {
						body : jsondata.data[0]['bodyText']
					});
				});
			}
		}
		if (jsondata.action === "putallstyles") {
			allStylesContent = document.getElementById("allStylesContent");
			allStylesContent.innerHTML = "";
			for (var i = 0; i < jsondata.data.length; i++) {
				allStylesContent.innerHTML += "<p style=\"background-color: " + jsondata.data[i]['bg_color']
					+ "; color: " + jsondata.data[i]['fg_color']
					+ ";\">"
					+ jsondata.data[i]['name']
					//+ ", (Expr.: " + decodeURI( jsondata.data[i]['expression'] ) + ")"
					+ " (" + jsondata.data[i]['expression'] + ")"
					+ "<input type=\"checkbox\" value=\"" + jsondata.data[i]['styleID'] + "\" /></p>";
			}
		}
		if (jsondata.action === "response") {
			if (jsondata.data[0]["text"] === "") {
				socket.close();
				location.reload();
			}
			else {
				showAlert(jsondata.data[0]["text"]);
			}
		}
	}
};

function send(sendString) {
	if (socket != undefined && socket.readyState === WebSocket.OPEN) {
		socket.send(sendString);
	}
	else {
		showAlert("No connection!")
	}
}