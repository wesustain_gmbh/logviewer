/*
 *  LogViewer - A small tool to observe your logfiles.
 *  Copyright (C) 2018/2019 Stefan Berning
 *  Copyright (C) 2018/2019 WeSustain GmbH
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.wesustain.logviewer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import com.wesustain.logviewer.config.Preferences;

import lombok.NonNull;


/**
 * @author sberning
 */
@SpringBootApplication
public class LogViewerApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(LogViewerApplication.class, args);
		new Preferences(); // To start as standallone
	}

	@NonNull
	@Override
	protected SpringApplicationBuilder configure(@NonNull SpringApplicationBuilder application) {
		new Preferences(); // To start in extern application-server
		return application;
	}
}