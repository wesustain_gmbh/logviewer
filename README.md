# LogViewer
Read your log files in the web browser from anywhere. Provides customizable syntax highlighting, filtering (e.g. for error messages) and a login option.

## HOW TO INSTALL

1. create a directory where your applicationserver allows the LogViewer to have resources
2. Put the 'preferences_example.xml' from the etc-directory to there and rename it to 'resources.xml'
3. Fill in the information which are right for you.

4. Use the firstrun.jar to create a database in the directory you created in step 1.
Usage: java -jar firstrun.jar </path/to/your/resources/directory/> <password>
After that you have 'LogViewer.db' with two tables and one first user with admin-role.
The default name is 'admin' and has your given password.
PLEASE NOTE: I recommend to change the default admin user to a unique other user and password.

5. Use 'maven package' to kreate a war-file from the LogViewer. Deploy it to your applicationserver.
Start the applicationserver with following options:
-Dresources.dir=</absolute/path/to/resources/for/the/LogViewer/>

Note: All given directories have to end with a '/'.

For start with the internal tomcat server over HTTPS a keystore and the server.ssl credentials are required in the application.properties.
See file application_example.properties

For use with Tomcat-Server:
Following lines have to be inserted into Tomcat-Server.xml:

```xml
        <!-- Need for CSRF-Protection  -->
        <!-- Mark HTTP as HTTPS forward from SSL termination at nginx proxy -->
        <Valve className="org.apache.catalina.valves.RemoteIpValve"
                remoteIpHeader="x-forwarded-for"
                remoteIpProxiesHeader="x-forwarded-by"
                protocolHeader="x-forwarded-proto"
        />
```

## License:

```
All images in this project are licensed under CC0 License
https://creativecommons.org/share-your-work/public-domain/cc0/

LogViewer - A small tool to observe your logfiles.
Copyright (C) 2018/2019 Stefan Berning
Copyright (C) 2018/2019 WeSustain GmbH

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

---------------------------------------------------------------------------------------------------------------------------------------------------------------